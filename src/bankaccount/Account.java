package bankaccount;

/**
 * Пример внутреннего класса
 * <p>
 * Нестатические вложенные классы называются внутренними.
 * Non-static nested classes are called inner classes.
 */
@SuppressWarnings("unused")
public class Account {
    private final String number;
    private final String owner;
    private int amount;

    public Account(final String number, final String owner) {
        this.number = number;
        this.owner = owner;
    }

    public String getNumber() {
        return number;
    }

    public String getOwner() {
        return owner;
    }

    public int getAmount() {
        return amount;
    }

    /**
     * Метод вывода с аккаунта
     *
     * @param withdrawMoney количество денег для вывода
     * @return сумма вывода
     * 0 - если вывод невозможен
     * {@code amount} - если сумма вывода больше баланса
     */
    private int withdraw(int withdrawMoney) {
        if (withdrawMoney < 0) {
            return 0;
        }
        if (withdrawMoney > amount) {
            return amount;
        }
        amount = amount - withdrawMoney;
        return withdrawMoney;
    }

    /**
     * Метод депозита на аккаунт
     *
     * @param depositMoney сумма депозита
     * @return сумма пополнения
     * 0 - если пополнение невозможно
     */
    private int deposit(int depositMoney) {
        if (depositMoney < 0) {
            return 0;
        }
        amount = amount + depositMoney;
        return depositMoney;
    }

    /**
     * Внутренний класс
     * Inner class
     */
    public class Card {
        private final String number;

        public Card(final String number) {
            this.number = number;
        }

        public String getNumber() {
            return number;
        }

        /**
         * Метод вывода с карты
         *
         * @param withdrawMoney количество денег для вывода
         * @return сумма вывода записанная на аккаунт
         */
        public int withdraw(final int withdrawMoney) {
            return Account.this.withdraw(withdrawMoney);
        }

        /**
         * Метод пополнения баланса карты
         *
         * @param depositMoney сумма для пополнения
         * @return сумма депозита записанная на аккаунт
         */
        public int deposit(int depositMoney) {
            return Account.this.deposit(depositMoney);
        }
    }
}