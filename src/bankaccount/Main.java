package bankaccount;

import java.util.Scanner;

/**
 * Класс, демонстрирующий работу банка
 *
 * @author Lvov Nikita 18it18
 */
public class Main {

    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        final Account accountVTB =
                new Account("1234554321",
                        "Nikita Lvov");
        final Account.Card cardVTB =
                accountVTB.new Card("2340 7845 8956 8906");

        final Account accountSberBank =
                new Account("5432112345",
                        "Alex Wash");
        final Account.Card cardSberBank_MIR =
                accountSberBank.new Card("8906 7845 8956 8906");

        final Account.Card cardSberBank_MasterCard =
                accountSberBank.new Card("8956 8906 8906 7845");

        bankOperations(accountVTB, cardVTB, accountSberBank, cardSberBank_MIR, cardSberBank_MasterCard);
    }

    /**
     * Метод банковских операций, позволяющий пополнить-вывести баланс с различных аккаунтов и карт(аккаунты и карты на
     * которых возможны операции перечислены в параметрах)
     *
     * @param accountVTB              аккаунт ВТБ
     * @param cardVTB                 карта ВТБ
     * @param accountSberBank         аккаунт сбербанк
     * @param cardSberBank_MIR        карта сбербанк МИР
     * @param cardSberBank_MasterCard кара сбербанк мастеркард
     */
    public static void bankOperations(Account accountVTB, Account.Card cardVTB, Account accountSberBank, Account.Card cardSberBank_MIR, Account.Card cardSberBank_MasterCard) {

        int account = chooseAccount();

        int retry;

        do {

            // Для аккаунта Сбербанк

            if (account == 1) {

                int card = chooseCard();

                int operation = chooseOperation();

                // Пополнение карты Мир
                if (card == 1 && operation == 1) {
                    depositCard(cardSberBank_MIR, accountSberBank);
                }
                // Вывод с карты Мир
                if (card == 1 && operation == 2) {
                    withdrawCard(cardSberBank_MIR, accountSberBank);
                }
                // Пополнение карты Мастеркард
                if (card == 2 && operation == 1) {
                    depositCard(cardSberBank_MasterCard, accountSberBank);
                }
                // Вывод с карты Мастеркард
                if (card == 2 && operation == 2) {
                    withdrawCard(cardSberBank_MasterCard, accountSberBank);
                }
            }

            // Для аккаунта ВТБ

            if (account == 2) {

                int operation = chooseOperation();

                // Пополенние
                if (operation == 1) {
                    depositCard(cardVTB, accountVTB);
                }
                // Вывод
                if (operation == 2) {
                    withdrawCard(cardVTB, accountVTB);
                }
            }

            // Цикл повторения операций

            System.out.println("Хотите продолжить операции? \n" +
                    "1 - Да \n" +
                    "2 - Нет");
            retry = sc.nextInt();

        } while (retry == 1);
    }

    /**
     * Метод, позволяющий выбрать аккаунт
     *
     * @return 1 - Сбербанк
     * 2 - ВТБ
     */
    private static int chooseAccount() {
        System.out.println("Выберите аккаунт \n" +
                "1 - Сбербанк \n" +
                "2 - ВТБ");
        return sc.nextInt();
    }

    /**
     * Метод, позволяющий выбрать карту
     *
     * @return 1 - Мир
     * 2 - MasterCard
     */
    private static int chooseCard() {
        System.out.println("Выберите карту \n" +
                "1 - Мир \n" +
                "2 - MasterCard");
        return sc.nextInt();
    }

    /**
     * Метод, позволяющий выбрать операцию
     *
     * @return 1 - Пополнение
     * 2 - Вывод
     */
    private static int chooseOperation() {
        System.out.println("Выберите операцию \n" +
                "1 - Пополнение \n" +
                "2 - Вывод");
        return sc.nextInt();
    }

    /**
     * Метод, который взаимодействует с пользователем и позволяет пополнить баланс
     *
     * @param card    карта
     * @param account счёт
     */
    private static void depositCard(Account.Card card, Account account) {
        System.out.println("Введите сумму для пополнения");

        int depositMoney = sc.nextInt();

        int result = card.deposit(depositMoney);

        if (result == 0) {
            System.out.println("Введена некорректная сумма для пополнения.");
        }

        balanceCard(account);
    }

    /**
     * Метод, который взаимодействует с пользователем и позволяет вывести деньги
     *
     * @param card    карта
     * @param account счёт
     */
    private static void withdrawCard(Account.Card card, Account account) {
        System.out.println("Введите суммы для вывода");
        int withdrawMoney = sc.nextInt();

        int result = card.withdraw(withdrawMoney);

        if (result == 0) {
            System.out.println("Введена некорректная сумма для снятия.");
        }
        if (result < withdrawMoney) {
            System.out.println("Была введена сумма для снятия больше чем на балансе.");
        }

        balanceCard(account);
    }

    /**
     * Метод выводит состояние баланса
     *
     * @param account счёт
     */
    private static void balanceCard(Account account) {
        System.out.println("Текущий баланс карты: " + account.getAmount());
    }
}