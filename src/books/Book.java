package books;

/**
 * Класс книг
 *
 * @author Nikita Lvov 18it18
 */
@SuppressWarnings("unused")
public class Book {
    private int year;
    private String title;
    private String author;
    private Genre genre;
    private String location;

    Book(int year, String title, String author, Genre genre, String location) {
        this.year = year;
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.location = location;
    }

    int getYear() {
        return year;
    }

    String getTitle() {
        return title;
    }

    String getAuthor() {
        return author;
    }

    Genre getGenre() {
        return genre;
    }

    public String getLocation() {
        return location;
    }

    @Override
    public String toString() {
        return "Книга{" +
                "Год издания:" + year +
                ", Название:'" + title + '\'' +
                ", Автор:'" + author + '\'' +
                ", Жанр:'" + genre + '\'' +
                ", Путь к файлу:'" + location + '\'' +
                '}';
    }
}


