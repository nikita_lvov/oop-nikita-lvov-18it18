package books;

/**
 * Класс жанров
 *
 * @author Nikita Lvov 18it18
 */
public enum Genre {
    CRIME,
    DETECTIVEFICTION,
    SCIENCEFICTION,
    POSTAPOCALYPTIC,
    DISTOPIA,
    CYBERPUNK,
    FANTASY,
    ROMANCE,
    GUIDE,
    UNKNOWN
}
