package books;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Класс для реализации сортировки, фильтрации, поиска книг
 *
 * @author Nikita Lvov 18it18
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        Book book1 = new Book(2017, "Grok algorithms", "Bhargava", Genre.GUIDE, "https://bitbucket.org/nikita_lvov/oop-nikita-lvov-18it18/wiki/Grok%20algorithms");
        Book book2 = new Book(1982, "Peace and war", "Tolstoy", Genre.ROMANCE, "https://bitbucket.org/nikita_lvov/oop-nikita-lvov-18it18/wiki");
        Book book3 = new Book(2002, "Hobbit", "Tolkin", Genre.FANTASY, "https://bitbucket.org/nikita_lvov/oop-nikita-lvov-18it18/wiki");
        Book[] books = {book1, book2, book3};

        System.out.println("Сортировка книг по году издания: ");
        sortYear(books);
        print(books);

        System.out.println("Сортировка книг по автору: ");
        sortAuthor(books);
        print(books);

        System.out.println("Введите название книги для поиска: ");
        String name = sc.nextLine();
        ArrayList<Book> namesBook = searchTitle(books, name);
        if (namesBook.isEmpty()) {
            System.out.println("Таких книг нет в списке");
        }
        System.out.println(namesBook);// поиск по названию

        System.out.println("Введите автора книги для фильтрации: ");
        String author = sc.nextLine();
        ArrayList<Book> authorsBook = filtrationAuthor(books, author);
        if (authorsBook.isEmpty()) {
            System.out.println("Таких книг нет в списке");
        }
        System.out.println(authorsBook);// фильтрация по автору книги

        Genre genre = Genre.UNKNOWN;
        System.out.println("Введите жанр книги для фильтрации:\n" +
                "1 - Научная фантастика\n" +
                "2 - Детектив\n" +
                "3 - Криминал\n" +
                "4 - Дистопия\n" +
                "5 - Обучающие книги\n" +
                "6 - Киберпанк\n" +
                "7 - Постапокалипс\n" +
                "8 - Фэнтези\n" +
                "9 - Роман\n ");
        int numberOfGender = sc.nextInt();
        if (numberOfGender == 1) {
            genre = Genre.SCIENCEFICTION;
        }
        if (numberOfGender == 2) {
            genre = Genre.DETECTIVEFICTION;
        }
        if (numberOfGender == 3) {
            genre = Genre.CRIME;
        }
        if (numberOfGender == 4) {
            genre = Genre.DISTOPIA;
        }
        if (numberOfGender == 5) {
            genre = Genre.GUIDE;
        }
        if (numberOfGender == 6) {
            genre = Genre.CYBERPUNK;
        }
        if (numberOfGender == 7) {
            genre = Genre.POSTAPOCALYPTIC;
        }
        if (numberOfGender == 8) {
            genre = Genre.FANTASY;
        }
        if (numberOfGender == 9) {
            genre = Genre.ROMANCE;
        }
        ArrayList<Book> genresBook = filtrationGenre(books, genre);
        if (genresBook.isEmpty()) {
            System.out.println("Таких книг нет в списке");
        }
        System.out.println(genresBook);// фильтрация по жанру книги

    }

    /**
     * Метод, реализующий сортировку книг по году(сначала новинки)
     *
     * @param books массив книг
     */
    private static void sortYear(Book[] books) {
        Book replace;
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < books.length - 1; i++) {
                if (books[i].getYear() < books[i + 1].getYear()) {
                    isSorted = false;

                    replace = books[i];
                    books[i] = books[i + 1];
                    books[i + 1] = replace;
                }
            }
        }
    }

    /**
     * Метод, реализующий сортировку книг по автору(по алфавиту)
     *
     * @param books массив книг
     */
    private static void sortAuthor(Book[] books) {
        for (int i = 1; i < books.length; i++) {
            Book books1 = books[i];
            int j = i;
            while (j > 0 && books[j - 1].getAuthor().compareTo(books1.getAuthor()) >= 0) {
                books[j] = books[j - 1];
                j--;
            }
            books[j] = books1;
        }
    }

    /**
     * Метод, реализующий поиск книг по названию
     *
     * @param books массив книг
     * @param name  введенное название книги
     * @return список книг присутствующих в массиве
     */
    private static ArrayList<Book> searchTitle(Book[] books, String name) {
        ArrayList<Book> namesBook = new ArrayList<>();
        for (Book book : books) {
            if (name.equals(book.getTitle())) {
                namesBook.add(book);
            }
        }
        return namesBook;
    }

    /**
     * Метод, реализующий вывод книг после сортировок
     *
     * @param books массив книг
     */
    private static void print(Book[] books) {
        for (Book book : books) {
            System.out.println(book);
        }
        System.out.println();
    }

    /**
     * Метод, реализующий фильтрацию книг по автору
     *
     * @param books  массив книг
     * @param author введенный автор книги
     * @return список книг отфильтрованный по автору
     */
    private static ArrayList<Book> filtrationAuthor(Book[] books, String author) {
        ArrayList<Book> authorsBook = new ArrayList<>();
        for (Book book : books) {
            if (author.equals(book.getAuthor())) {
                authorsBook.add(book);
            }
        }
        return authorsBook;
    }

    /**
     * Метод, реализующий фильтрацию книг по жанру
     *
     * @param books массив книг
     * @param genre введенный жанр книг
     * @return список книг отфильтрованный по жанру
     */
    private static ArrayList<Book> filtrationGenre(Book[] books, Genre genre) {
        ArrayList<Book> genresBook = new ArrayList<>();
        for (Book book : books) {
            if (genre.equals(book.getGenre())) {
                genresBook.add(book);
            }
        }
        return genresBook;
    }
}
