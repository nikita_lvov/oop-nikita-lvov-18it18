package cafe;

import cafe.topping.Sprinkling;
import cafe.topping.Syrup;
import cafe.product.Component;
import cafe.product.IceCream;
import cafe.product.Smoothie;
import cafe.product.Yogurt;

import java.util.Random;

public class Demo {
    public static void main(String[] args) {
        Component IceCream;
        Component Smoothie;
        Component Yogurt;
        Random random = new Random();
        boolean randomSell = random.nextBoolean();
        if (randomSell) {
            IceCream = new Sprinkling(new IceCream());
            Smoothie = new Syrup(new Smoothie());
            Yogurt = new Sprinkling(new Syrup(new Yogurt()));
        } else {
            IceCream = new IceCream();
            Smoothie = new Smoothie();
            Yogurt = new Yogurt();
        }
        IceCream.sell();
        Smoothie.sell();
        Yogurt.sell();
    }
}
