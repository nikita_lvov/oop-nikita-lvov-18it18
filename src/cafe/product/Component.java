package cafe.product;

public interface Component {
    void sell();
}
