package cafe.topping;

import cafe.product.Component;

public abstract class Decorator implements Component {
    private Component component;

    Decorator(Component component) {
        this.component = component;
    }

    public abstract void topping();

    @Override
    public void sell() {
        component.sell();
        topping();
    }
}
