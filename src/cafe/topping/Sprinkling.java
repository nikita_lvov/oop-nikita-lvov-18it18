package cafe.topping;

import cafe.product.Component;

public class Sprinkling extends Decorator {
    public Sprinkling(Component component) {
        super(component);
    }

    @Override
    public void topping() {
        System.out.println("Добавлена присыпка");
    }
}
