package cafe.topping;

import cafe.product.Component;

public class Syrup extends Decorator {
    public Syrup(Component component) {
        super(component);
    }

    @Override
    public void topping() {
        System.out.println("Полито сиропом");
    }
}
