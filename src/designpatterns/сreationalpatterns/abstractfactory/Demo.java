package designpatterns.˝reationalpatterns.abstractfactory;

import designpatterns.˝reationalpatterns.abstractfactory.factory.RussianFactory;
import designpatterns.˝reationalpatterns.abstractfactory.factory.TransportFactory;
import designpatterns.˝reationalpatterns.abstractfactory.factory.USAFactory;
import designpatterns.˝reationalpatterns.abstractfactory.transport.Airplane;
import designpatterns.˝reationalpatterns.abstractfactory.transport.Car;

import java.util.Random;

public class Demo {
    public static void main(String[] args) {
        Random random = new Random();
        boolean flag = random.nextBoolean();
        TransportFactory transportFactory;
        if (flag) {
            transportFactory = new RussianFactory();
        } else {
            transportFactory = new USAFactory();
        }
        transportFactory.createCar();
        transportFactory.createAirplane();

        Car car = transportFactory.createCar();
        car.drive();
        car.stop();

        Airplane airplane = transportFactory.createAirplane();
        airplane.flight();

    }
}
