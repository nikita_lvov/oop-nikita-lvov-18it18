package designpatterns.˝reationalpatterns.abstractfactory.factory;

import designpatterns.˝reationalpatterns.abstractfactory.transport.Airplane;
import designpatterns.˝reationalpatterns.abstractfactory.transport.Car;
import designpatterns.˝reationalpatterns.abstractfactory.transport.Niva;
import designpatterns.˝reationalpatterns.abstractfactory.transport.TU204;

public class RussianFactory implements TransportFactory {
    @Override
    public Car createCar() {
        return new Niva();
    }

    @Override
    public Airplane createAirplane() {
        return new TU204();
    }
}