package designpatterns.˝reationalpatterns.abstractfactory.factory;

import designpatterns.˝reationalpatterns.abstractfactory.transport.Airplane;
import designpatterns.˝reationalpatterns.abstractfactory.transport.Car;

public interface TransportFactory {
    Car createCar();

    Airplane createAirplane();
}