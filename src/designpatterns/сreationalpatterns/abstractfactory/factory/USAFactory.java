package designpatterns.˝reationalpatterns.abstractfactory.factory;

import designpatterns.˝reationalpatterns.abstractfactory.transport.Airplane;
import designpatterns.˝reationalpatterns.abstractfactory.transport.Boeing747;
import designpatterns.˝reationalpatterns.abstractfactory.transport.Car;
import designpatterns.˝reationalpatterns.abstractfactory.transport.Ford;

public class USAFactory implements TransportFactory {
    @Override
    public Car createCar() {
        return new Ford();
    }

    @Override
    public Airplane createAirplane() {
        return new Boeing747();
    }
}
