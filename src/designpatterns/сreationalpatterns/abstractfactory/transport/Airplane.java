package designpatterns.˝reationalpatterns.abstractfactory.transport;

public interface Airplane {
    void flight();
}
