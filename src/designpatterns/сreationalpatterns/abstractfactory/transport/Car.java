package designpatterns.˝reationalpatterns.abstractfactory.transport;

public interface Car {
    void drive();

    void stop();
}