package designpatterns.˝reationalpatterns.factorymethod;

public interface Car {
    void drive();

    void stop();
}