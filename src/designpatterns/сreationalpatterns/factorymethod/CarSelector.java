package designpatterns.�reationalpatterns.factorymethod;

class CarSelector {

    /**
     * ��������� �����, ������� ������� ������ ����������
     *
     * @param roadType ��� ������
     * @return ���������� � ����������� �� ���� ������
     */
    Car getCar(RoadType roadType) {
        Car car = null;
        switch (roadType) {
            case CITY:
                car = new Renault();
                break;
            case OFF_ROADS:
                car = new Geep();
                break;
            case TRACK:
                car = new Ferrari();
                break;
        }
        return car;
    }
}