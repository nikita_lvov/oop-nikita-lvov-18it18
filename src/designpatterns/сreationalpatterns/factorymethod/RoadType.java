package designpatterns.˝reationalpatterns.factorymethod;

public enum RoadType {
    CITY,
    OFF_ROADS,
    TRACK
}