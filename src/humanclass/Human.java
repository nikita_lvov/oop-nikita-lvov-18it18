package humanclass;

/**
 * Пример вложенного класса
 * <p>
 * Вложенные классы, объявленные статическими,
 * называются статическими вложенными классами.
 * Nested classes that are declared static are called static nestedwithenum classes.
 */
public class Human {
    private String name;
    private int age;
    private Relation relation;

    /**
     * Вложенный класс
     * Nested (static) class
     */
    static class Relation {
        private String name;

        Relation(String name) {
            this.name = name;
        }

        String getName() {
            return name;
        }
    }

    Human(String name, int age, Relation relation) {
        this.name = name;
        this.age = age;
        this.relation = relation;
    }

    Relation getRelation() {
        return relation;
    }

    void setRelation(Relation relation) {
        this.relation = relation;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", relation=" + relation.getName() +
                '}';
    }
}