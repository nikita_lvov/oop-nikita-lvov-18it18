﻿package humanclass;

public class Main {
    public static void main(String[] args) {
        Human man = new Human("Василий", 20, new Human.Relation("одинок"));
        Human woman = new Human("Василиса", 20, new Human.Relation("влюблена"));
        System.out.println(man);
        System.out.println(woman);
        System.out.println(man.getRelation().getName());
        System.out.println(woman.getRelation().getName());

        man.setRelation(new Human.Relation("влюблен"));
        Human.Relation relation = new Human.Relation("в активном поиске");
        woman.setRelation(relation);
        System.out.println(man);
        System.out.println(woman);
    }
}