package humanenum;

public class Main {
    public static void main(String[] args) {
        Human man = new Human("Denis", 18, Human.Relations.SINGLE);
        Human woman = new Human("Nadya", 21, Human.Relations.SINGLE);
        System.out.println(man);
        System.out.println(woman);

        man.setRelations(Human.Relations.IN_LOVE);
        woman.setRelations(Human.Relations.MARRIED);
        System.out.println(woman);
        woman.setRelations(Human.Relations.DIVORCED);
        System.out.println(man);
        System.out.println(woman);
    }
}