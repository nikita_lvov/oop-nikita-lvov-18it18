package interfaces;

import interfaces.printers.AdvConsolePrinter;
import interfaces.printers.IPrinter;
import interfaces.printers.StarsPrinter;
import interfaces.readers.IReader;
import interfaces.readers.ScannerReader;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        IReader scReader = new ScannerReader();

        // IReader reader = new PredefinedReader("Привет :) Пока-пока:)");
        // IPrinter printer = new ConsolePrinter();
        IPrinter advPrinter = new AdvConsolePrinter();
        IPrinter starPrinter = new StarsPrinter();
        // Replacer replacer = new Replacer(reader,printer);
        Replacer advReplacer = new Replacer(scReader,advPrinter);

        // replacer.replace();
        advReplacer.replace();

    }
}
