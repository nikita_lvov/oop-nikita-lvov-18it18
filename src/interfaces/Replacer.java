package interfaces;

import interfaces.printers.IPrinter;
import interfaces.readers.IReader;

import java.io.IOException;

class Replacer {
    private IReader reader;
    private IPrinter printer;

    Replacer(IReader reader, IPrinter printer) {
        this.reader = reader;
        this.printer = printer;
    }

    void replace() throws IOException {
        final String text = reader.read();
        final String replacedText = text.replaceAll("[:)]", ":(");
        printer.print(replacedText);
    }
}
