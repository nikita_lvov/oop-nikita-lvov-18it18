package interfaces.printers;

public class StarsPrinter implements IPrinter {
    @Override
    public void print(final String text) {
        System.out.println("* * * " + text + " * * * ");
    }
}
