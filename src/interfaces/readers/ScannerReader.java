package interfaces.readers;

import java.util.Scanner;

public class ScannerReader implements IReader {

    @Override
    public String read() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите текст: ");
        return sc.nextLine();
    }
}
