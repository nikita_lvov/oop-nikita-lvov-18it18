package obfuscation;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
// import java.util.Scanner;

/**
 * Мини-обфускатор
 * реализует:
 * удаление однострочных и многострочных комментариев
 * удаление лишних пробелов и переноса на новую строку
 *
 * @author Nikita Lvov 18it18
 */
public class Obfuscation {
    public static void main(String[] args) throws IOException {
        /*
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите путь к файлу");
        String filePath = sc.nextLine();
         */
        String filePath = "E:\\Work\\Chocolate.java"; // путь к файлу
        String regexFile = readUsingBufferedReader(filePath); // считанный файл
        String nameFile = findNameFile(filePath); // название класса(имя файла)
        String finalFile = regex(regexFile, nameFile); // обфусцированный файл
        writeFile(nameFile, finalFile); // запись обфусцированного файла
    }

    /**
     * Метод, реализующий считывание с файла
     *
     * @param filePath путь к файлу
     * @return считанный файл преобразуется в строки
     */
    private static String readUsingBufferedReader(String filePath) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        String line;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }

        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }

    /**
     * Поиск названия класса java файла
     *
     * @param filePath путь к файлу
     * @return название класса
     */
    private static String findNameFile(String filePath) {
        filePath = filePath.replaceAll("\\.java", "");
        ArrayList<String> nameFiles = new ArrayList<>(Arrays.asList(filePath.split("\\\\")));
        return nameFiles.get(nameFiles.size() - 1);
    }

    /**
     * Метод преобразования файла(обфускации)
     *
     * @param regexFile файл, который надо обфусцировать
     * @param nameFile  название класса java файла
     * @return обфусцированный java файл
     */
    private static String regex(String regexFile, String nameFile) {
        regexFile = regexFile.replaceAll("\\s{2,}", "");// удаление лишних пробелов и перехода на новую строку
        regexFile = regexFile.replaceAll("(/\\*([^*]|[\\r\\n]|(\\*+([^*/]|[\\r\\n])))*\\*+/)", "");// удаление многострочных комментариев
        regexFile = regexFile.replaceAll("(/\\*.+?\\*/)|(//.+?)[:;a-zA-Za-яА-ЯЁё]*", "");// удаление однострочных комментариев
        regexFile = regexFile.replaceAll(nameFile, "New" + nameFile);// замена старого имя на новое с приставкой New
        return regexFile;
    }

    /**
     * Метод, реализующий присваивание названию файла приставки New
     * и запись в новый файл
     *
     * @param nameFile  название файла
     * @param finalFile обфусцированный файл
     */
    private static void writeFile(String nameFile, String finalFile) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("New" + nameFile))) {
            bw.write(finalFile);
        } catch (IOException ignored) {

        }
    }
}