package obfuscation2;

import java.io.IOException;
import java.util.Scanner;

/**
 * Класс Demo для реализации обфускации
 *
 * @author Nikita Lvov 18it18
 */
public class Demo {
    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите путь к файлу");
        String filePath = sc.nextLine();
        String nameFile = Obfuscation.findNameFile(filePath);
        String newFilePath = filePath.replaceAll(nameFile, "New" + nameFile);
        System.out.println("Новый путь к файлу - " + newFilePath);
        FileObfuscator newFile = new FileObfuscator(filePath, newFilePath);
        newFile.obfuscate();
        System.out.println("Файл был обфусцирован!");
/*
         String filePath = "E:\\Work\\Chocolate.java";
         FileObfuscator newFile = new FileObfuscator("", "E:\\Work\\NewChocolate.java");
         newFile.obfuscate(filePath);

 */
    }
}
