package obfuscation2;

import java.io.IOException;

/**
 * ����� �����, ��������� ����������� ������ Obfuscation
 *
 * @author Nikita Lvov 18it18
 */
class FileObfuscator {
    private String filePath;
    private String newFilePath;

    FileObfuscator(String filePath, String newFilePath) {
        this.filePath = filePath;
        this.newFilePath = newFilePath;
    }

    /**
     * ����� ����������,���������� � ���� ������ ������ Obfuscation � ����������� �������������� �����
     */
    void obfuscate() throws IOException {
        String file = Obfuscation.readFile(filePath);
        String nameFile = Obfuscation.findNameFile(filePath);
        String obfuscateFile = Obfuscation.obfuscateFile(file);
        String finalFile = Obfuscation.changeName(obfuscateFile, nameFile);
        Obfuscation.writeFile(nameFile, finalFile);
    }

    @SuppressWarnings("unused")
    public String getFilePath() {
        return filePath;
    }

    @SuppressWarnings("unused")
    public String getNewFilePath() {
        return newFilePath;
    }
}

