package obfuscation2;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * ����-����������
 * ���������:
 * �������� ������������ � ������������� ������������;
 * �������� ������ �������� � �������� �� ����� ������
 *
 * @author Nikita Lvov 18it18
 */
class Obfuscation {

    /**
     * �����, ����������� ���������� � �����
     *
     * @param filePath ���� � �����
     * @return ��������� ���� ������������� � ������
     */
    static String readFile(String filePath) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        String line;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }

        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }

    /**
     * ����� �������� ������ java �����
     *
     * @param filePath ���� � �����
     * @return �������� ������
     */
    static String findNameFile(String filePath) {
        filePath = filePath.replaceAll("\\.java", "");
        ArrayList<String> nameFiles = new ArrayList<>(Arrays.asList(filePath.split("\\\\")));
        return nameFiles.get(nameFiles.size() - 1);
    }

    /**
     * ����� �������������� �����(����������)
     *
     * @param file ����, ������� ���� �������������
     * @return ��������������� ����
     */
    static String obfuscateFile(String file) {
        file = file.replaceAll("(/\\*.+?\\*/)|(//.+?)[:;a-zA-Za-��-ߨ�]*", "");// �������� ������������ ������������
        file = file.replaceAll("\\s{2,}", "");// �������� ������ �������� � �������� �� ����� ������
        file = file.replaceAll("(/\\*([^*]|[\\r\\n]|(\\*+([^*/]|[\\r\\n])))*\\*+/)", "");// �������� ������������� ������������

        return file;
    }

    /**
     * ����� ������ �����
     *
     * @param file     ����
     * @param nameFile ��� �����
     * @return ���� � ����������� �������
     */
    static String changeName(String file, String nameFile) {
        file = file.replaceAll(nameFile, "New" + nameFile);// ������ ������� ��� �� ����� � ���������� New

        return file;
    }

    /**
     * �����, ����������� ������ � ����� ���� � ������������ �������� ����� ��������� New
     *
     * @param nameFile  �������� �����
     * @param finalFile ��������������� ����
     */
    static void writeFile(String nameFile, String finalFile) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("New" + nameFile))) {
            bw.write(finalFile);
        } catch (IOException ignored) {

        }
    }
}
