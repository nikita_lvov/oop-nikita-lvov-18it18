package regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegID {
    public static void main(String[] args) {

        final String regex = "^[A-z_]*[a-z_][\\dA-z_-]*$";

        final Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

        final Matcher matcher;

        {
            String string = "Тестовые данные:\n"
                    + "a1\n"
                    + "1\n"
                    + "-a2\n"
                    + "_a\n"
                    + "z\n"
                    + "-\n"
                    + "_\n\n";
            matcher = pattern.matcher(string);
        }

        while (matcher.find()) {
            System.out.println("Full match: " + matcher.group(0));
            for (int i = 1; i <= matcher.groupCount(); i++) {
                System.out.println("Group " + i + ": " + matcher.group(i));
            }
        }
    }
}