package regex;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс, читающий файл и отбирающий приставки пре-при
 *
 * @author Nikita Lvov 18it18
 */
public class SearchPrefix {
    public static void main(String[] args) throws IOException {
        String s;
        Pattern pattern = Pattern.compile("\\b(пре|при|При|Пре)[а-яё]+\\b");
        Matcher matcher = pattern.matcher("");

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\regex\\input.txt"))) {
            while ((s = bufferedReader.readLine()) != null) {
                matcher.reset(s);
                while (matcher.find()) {
                    System.out.println(matcher.group());
                }
            }
        }
    }
}



